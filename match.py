from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pandas as pd 
from time import sleep
import pickle

stakes = [0.35,0.36,0.37,0.38,0.43,0.48,0.54,0.61,0.68,0.77,0.86,0.97,1.09,1.22,1.38,1.55,1.74,1.95,2.19,2.46]

driver  = webdriver.Chrome('/home/srinivas/Downloads/chromedriver_linux64/chromedriver')
url = 'https://www.binary.com/en/trading.html?currency=USD&market=volidx&underlying=R_25&formname=matchdiff&duration_amount=1&duration_units=t&amount={}&amount_type=stake&expiry_type=duration&prediction=2'.format(stakes[0])
driver.get(url)


list_index = 1
i = 1;
sleep(30)
flag = False
prev_index = 0
while i>0 and (list_index<len(stakes)):

    try:
        driver.execute_script('$("#close_confirmation_container").click()')
        index = driver.find_elements_by_xpath('//*[@id="spot"]')[0]
        index_value = index.text
        bg_color = driver.execute_script('return $("#spot").css("background-color")') 
        if (index_value == prev_index) and (bg_color != 'rgb(255, 255, 255)'):
            continue
        print(index_value)
        if index_value[len(index_value)-1] == '2':
            if flag == True:
                print("Contract Won")
                list_index = 0
                curr_stake = stakes[list_index]
                print(curr_stake)
                script = '$("#amount").val({})'.format(curr_stake)
                driver.execute_script(script)
                list_index+=1
                flag = False
            
            elif flag == False:
                
                driver.execute_script('$("#purchase_button_top").click()')
                driver.execute_script('$("#close_confirmation_container").click()')
                flag = True
    
        else:
            if flag == True:
                print("Contract lost")
                curr_stake = stakes[list_index]
                print(curr_stake)
                script = '$("#amount").val({})'.format(curr_stake)
                driver.execute_script(script)
                list_index+=1
                flag = False
            
        prev_index = index_value
    except IndexError:
        continue
            
    

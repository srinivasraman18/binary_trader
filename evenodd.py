from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pandas as pd 
from time import sleep
import pickle
import json

stakes = [0.50,1.00,2.00,4.50,9.20,19.00,38.80,80.00,165.50,351.00]

driver  = webdriver.Chrome('/home/srinivas/Downloads/chromedriver_linux64/chromedriver')
url = 'https://www.binary.com/en/trading.html?currency=USD&market=volidx&underlying=R_25&formname=evenodd&duration_amount=1&duration_units=t&amount={}&amount_type=stake&expiry_type=duration'.format(stakes[0])
driver.get(url)


list_index = 1
i = 1;
sleep(30)
flag = False
prev_index = 0
text_change= str(stakes[0])
data_change = str(stakes[0])

while i>0 and (list_index<len(stakes)):

    try:
        driver.execute_script('$("#close_confirmation_container").click()')
        if  flag==False:
            driver.execute_script(text_change)
            driver.execute_script(data_change)


        index = driver.find_elements_by_xpath('//*[@id="spot"]')[0]
        index_value = index.text
        bg_color = driver.execute_script('return $("#spot").css("background-color")') 
        if (index_value == prev_index) and (bg_color != 'rgb(255, 255, 255)'):
            continue
        print(index_value)
        if int(index_value[len(index_value)-1])%2 == 0:
            if flag == True:
                print("Contract Won")
                list_index = 0
                curr_stake = stakes[list_index]
                print(curr_stake)
                return_percentage_element= driver.find_elements_by_xpath('//*[@id="price_container_top"]/div[2]')
                return_percentage = return_percentage_element[0].text
                if return_percentage!='':
                    return_percentage = float(return_percentage[26:30])/100
                    payout = curr_stake + curr_stake * return_percentage
                    script = '$("#amount").val({})'.format(curr_stake)
                    text_change = '$(".contract_amount").text({})'.format(curr_stake)
                    my_dict = {'data-amount':str(curr_stake),'data-display_value':str(curr_stake),'data-ask-price':str(curr_stake),'data-payout':str(payout)}
                    my_dict = json.dumps(my_dict)
                    data_change = '$("#purchase_button_top").attr({})'.format(my_dict)
                    driver.execute_script(script)
                    driver.execute_script(text_change)
                    driver.execute_script(data_change)
                    list_index+=1
                    flag = False
                    
           
                
            else:
                
                driver.execute_script('$("#purchase_button_top").click()')
                sleep(1)
                status = driver.execute_script('return $("#contract_confirmation_container").attr("style")')
                while status == 'display: block;':
                    driver.execute_script('$("#close_confirmation_container").click()')
                flag = True
    
        else:
            if flag == True:
                print("Contract lost")
                curr_stake = stakes[list_index]
                print(curr_stake)

                return_percentage_element= driver.find_elements_by_xpath('//*[@id="price_container_top"]/div[2]')
                return_percentage = return_percentage_element[0].text
                if return_percentage!='':
                    return_percentage = float(return_percentage[26:30])/100
                    payout = curr_stake + curr_stake * return_percentage
                    script = '$("#amount").val({})'.format(curr_stake)
                    text_change = '$(".contract_amount").text({})'.format(curr_stake)
                    my_dict = {'data-amount':str(curr_stake),'data-display_value':str(curr_stake),'data-ask-price':str(curr_stake),'data-payout':str(payout)}
                    my_dict = json.dumps(my_dict)
                    data_change = '$("#purchase_button_top").attr({})'.format(my_dict)
                    driver.execute_script(script)
                    driver.execute_script(text_change)
                    driver.execute_script(data_change)
                    list_index+=1
                    flag = False
                    
        prev_index = index_value
    except IndexError:
        continue
            
    
